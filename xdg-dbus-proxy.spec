Name:           xdg-dbus-proxy
Version:        0.1.2
Release:        1
Summary:        Filtering proxy for D-Bus connections
License:        LGPLv2+
URL:            https://github.com/flatpak/xdg-dbus-proxy/
Source0:        https://github.com/flatpak/xdg-dbus-proxy/releases/download/%{version}/%{name}-%{version}.tar.xz

BuildRequires:  docbook-style-xsl gcc glib2-devel libxslt 
Requires:       dbus

%description
xdg-dbus-proxy is a filtering proxy for D-Bus connections. It was originally
part of the flatpak project, but it has been broken out as a standalone module
to facilitate using it in other contexts.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install

%files
%defattr(-,root,root)
%license COPYING
%{_bindir}/xdg-dbus-proxy

%files help
%defattr(-,root,root)
%{_mandir}/man1/xdg-dbus-proxy.1*

%changelog
* Thu Jul 23 2020 songnannan <songnannan2@huawei.com> - 0.1.2-1
- package init
